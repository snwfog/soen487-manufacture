/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package manufacture;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import model.Manufacture;
import model.MiBookPro;

/**
 *
 * @author snw
 */
@WebService(serviceName = "ManufactureWS")
@Stateless()
public class ManufactureWS {
	/**
	 * Web service operation
	 */
	@WebMethod(operationName = "inquiry")
	public MiBookPro inquiry() {
		return (MiBookPro.getDescription());
	}

	/**
	 * Web service operation
	 */
	@WebMethod(operationName = "shipToWarehouse")
	public int shipToWarehouse(Integer amount) {
		Integer shipAmount = amount;
		Manufacture thisManufacture = Manufacture.getInstance();
		if (thisManufacture.getQuantity() < amount)
		{
			create();
			shipAmount = thisManufacture.getQuantity();
			thisManufacture.setQuantity(0);
		}
		else
		{
			shipAmount = amount;
			thisManufacture.setQuantity(thisManufacture.getQuantity() - amount);
		}

		return shipAmount;
	}

	/**
	 * Web service operation
	 */
	@WebMethod(operationName = "create")
	public Boolean create() {
		Manufacture thisManufacture = Manufacture.getInstance();
		thisManufacture.setQuantity(thisManufacture.getQuantity() 
						+ Manufacture.BATCH_CREATE_AMOUNT);
		
		return true;
	}

	/**
	 * Web service operation
	 */
	@WebMethod(operationName = "hasOnProductionLine")
	public Boolean hasOnProductionLine() {
		//TODO write your implementation code here:
		return null;
	}

	/**
	 * Web service operation
	 */
	@WebMethod(operationName = "getETAOnProductionLine")
	public Float getETAOnProductionLine() {
		//TODO write your implementation code here:
		return null;
	}

	// Stub...
	private float calculateShipTimeTo(String zip)
	{
		return 100.0f;
	}

	/**
	 * Web service operation
	 */
	@WebMethod(operationName = "manufactureInfo")
	public Manufacture manufactureInfo() {
		return Manufacture.getInstance();
	}
}
