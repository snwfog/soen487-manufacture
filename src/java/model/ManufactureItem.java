/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author snw
 */
public abstract class ManufactureItem {
	protected String name;
	protected float cost;
	protected float dimensionWeightFactor;
	protected int timeToProduce;

	protected ManufactureItem() {}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public float getDimensionWeightFactor() {
		return dimensionWeightFactor;
	}

	public void setDimensionWeightFactor(float dimensionWeightFactor) {
		this.dimensionWeightFactor = dimensionWeightFactor;
	}

	public int getTimeToProduce() {
		return timeToProduce;
	}

	public void setTimeToProduce(int timeToProduce) {
		this.timeToProduce = timeToProduce;
	}	

	@Override
	public String toString() {
		return "model.MiBookPro"
						+ "[ cost=" + cost + "]"
						+ "[ dimensionWeightFactor=" + dimensionWeightFactor + "]"
						+ "[ timeToProduce=" + timeToProduce + "]"
						+ "[ cost=" + cost + "]";
	}
	
}
