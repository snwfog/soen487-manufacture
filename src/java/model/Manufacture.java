/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author snw
 */
public class Manufacture {
	public static final int MIN_THRESHOLD_TRIGGER = 20;
	public static final int BATCH_CREATE_AMOUNT = 100;
	private String name;
	private String zip;
	private Integer quantity;

	private static Manufacture instance;

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	protected Manufacture() {}
	public static Manufacture getInstance()
	{
		if (instance == null)
		{
			Manufacture man = new Manufacture();
			man.name = "Punjabi";
			man.zip = "73301";
			man.quantity = 0;
			instance = man;
		}
		
		return instance;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}
}
